FROM php:7.2-fpm

RUN         apt-get update && \
            docker-php-ext-install pdo_mysql && \
            apt-get install -y libfreetype6-dev libjpeg62-turbo-dev && \
            docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/  &&  \
            docker-php-ext-install gd

RUN     apt-get install -y curl git unzip libxslt-dev \
        && curl -sS https://getcomposer.org/installer -o composer-setup.php \
        && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
        && docker-php-ext-install xsl \
        && docker-php-ext-install simplexml \
        && docker-php-ext-install xml \
        && docker-php-ext-install mbstring


RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    libzip-dev

RUN docker-php-ext-install zip


WORKDIR /var/www/forum-tdd
