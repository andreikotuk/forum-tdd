FROM nginx

ADD forum-tdd/docker/conf/nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /var/www/forum-tdd

RUN apt-get update \
    #&& docker-php-ext-install mbstring \
    && apt-get install -y curl git unzip \
    && curl -sS https://getcomposer.org/installer -o composer-setup.php
